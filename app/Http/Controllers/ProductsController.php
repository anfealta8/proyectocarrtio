<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function products()
    {
        $products =Product::all();

        return view('products',compact('products'));
    }

    public function detail($id)
    {
        $products =Product::find($id);

        return view('detail')->with('products',$products)
        ;
        
    }

public function cart()
{

    return view('cart');
}

public function addToCart($id)
{

    $product= Product::find($id);
    $cart = session()->get('cart');


    if(!$cart){

$cart = [
    $id => [
        "name"=>$product->name,
        "quantity" => 1,
        "price" => $product->price,
        "photo"=> $product->photo
    ]
];

session()->put('cart',$cart);

return redirect()->back()->with('success','Producto added to cart successfully!');

    }
   if(isset($cart[$id])){

    $cart[$id]['quantity']++;
    session()->put('cart',$cart);

    return redirect()->back()->with('success','Product added to cart sucessfully!');
   } 
$cart[$id]=[
    "name"=>$product->name,
    "quantity"=> 1,
    "price"=>$product->price,
    "photo"=> $product->phtoto
    ];

session()->put('cart',$cart);
return redirect()->back()->with('success','Product added to cart sucessfully!');

}

}
