<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('cliente', function () {
    
    
    
    $cliente = App\Models\Product::find(2);
    echo $cliente->price;

    //return view('Usuarios');
    //->with('datos',$datos);
});

Route::get('cart', function () {
    
    return view('cart');
    
       //return view('Usuarios');
    //->with('datos',$datos);
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/products', [App\Http\Controllers\ProductsController::class, 'products'])->name('products');
Route::get('/product-detail/{id}', [App\Http\Controllers\ProductsController::class, 'detail'])->name('detail');
Route::get('/add-to-cart/{id}', [App\Http\Controllers\ProductsController::class, 'addToCart'])->name('cart');
Route::get('/enviarcorreo', [App\Http\Controllers\MailController::class, 'getMail'])->name('pay');

//Route::get('/products', 'ProductsController@products')->name('products');