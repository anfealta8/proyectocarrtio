@extends('layouts.main', ['activePage' => 'dashboard', 'titlePage' => __('Detalles del producto')])


@section('content')
<div class="content">
    <div class="container-fluid">
    <a href="{{url('products')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Productos
</a>
        <div class="row">
            
        <div class="row">
        
 
 <div class="col-6">
 <img src="{{$products->photo}}" width="300" height="300">
 <h4>{{$products->name}} </h4>
 <p>{{strtolower($products->description)}} </p>
 <p><Strong>Precio: $</Strong>{{($products->price)}} </p>
</div>

</div>



        </div>
    </div>
</div>
@endsection
