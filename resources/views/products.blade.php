@extends('layouts.main', ['activePage' => 'dashboard', 'titlePage' => __('Productos')])

@section('content')
<div class="content">
    <div class="container-fluid">
        
    <div class="row">
        <a href="{{url('cart')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Carrito de compras
</a>


 @foreach($products as $product)
 <div class="col-6">
 <img src="{{$product->photo}}" width="300" height="300">
 <h4>{{$product->name}} </h4>
 <p>{{strtolower($product->description)}} </p>
 <p><Strong>Precio: $</Strong>{{($product->price)}} </p>

<a href="{{url('add-to-cart/'.$product->id)}}" class="btn btn-primary btn-lg btn-block" role="button" aria-pressed="true">
    Agrgar al Carrito
</a>
<a href="{{url('product-detail/'.$product->id)}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Detalles
</a>
</div>
@endforeach

</div>
 

        </div>
    
</div>
@endsection
