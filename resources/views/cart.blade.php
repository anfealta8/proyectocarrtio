@extends('layouts.main', ['activePage' => 'dashboard', 'titlePage' => __('Carrito de Compras')])


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <a href="{{url('products')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Productos
</a>
            
        <?php $valor = 0  ?>

        @if(session('cart'))

        <table class="table">
        <thead class="thead-dark">
        <tr>
        <th>
            Producto
    </th>
    <th>
            Precio Unitario
    </th>
    <th>
            Cantidad
    </th>
    <th>
            Precio Total
    </th>
    <th>
            Foto
    </th>
    
</tr>
</thead>

        @foreach(session('cart') as $id => $details)

        <?php $valor += $details['price']*$details['quantity'];?>
        <tr>
        <th>
        {{ $details['name'] }}
</th>
<th>
        S./{{$details['price']}}
</th>    
<th>
        {{$details['quantity']}}
        </th>
        <th>
        ${{ $details['price']*$details['quantity']}}
        </th>
        <th>
        <img src="{{$details['photo']}}" width="50" height="50" />
        </th>

</tr>

@endforeach
</table>
@endif
<table  align="right">
<th>
<div class="badge badge-primary text-wrap" style="width: 10rem;">
<p></p>
<p>Valor S./{{$valor}}</p>
<p>IGV S./{{$valor*0.18}}</p>
<p>Total S./{{$valor+$valor*0.18}}</p>
</div>
</th>
</table>

<a href="{{url('enviarcorreo')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Realizar compra
</a>

        </div>
    </div>
</div>
@endsection
