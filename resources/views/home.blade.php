@extends('layouts.main', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <a href="{{url('products')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">
    Productos
</a>
        </div>
    </div>
</div>
@endsection
